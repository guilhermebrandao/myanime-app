package it.codenize.myanimeapp.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import it.codenize.myanimeapp.R
import it.codenize.myanimeapp.model.UpcomingAnime
import kotlinx.android.synthetic.main.upcoming_anime.view.*


class UpcomingAnimesAdapter (private val mUpcomingAnimes: List<UpcomingAnime>,
                             private val context: Context) : RecyclerView.Adapter<UpcomingAnimesAdapter.UpcomingAnimeViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpcomingAnimeViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.upcoming_anime, parent, false)

        return UpcomingAnimeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mUpcomingAnimes.size
    }

    override fun onBindViewHolder(holderUpcomingAnime: UpcomingAnimeViewHolder, position: Int) {
        holderUpcomingAnime.bindView(mUpcomingAnimes[position])
    }

    class UpcomingAnimeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(upcomingAnime: UpcomingAnime) {
            val title = itemView.upcoming_anime_item_title
            val year = itemView.upcoming_anime_item_year


            val airingStart = if (upcomingAnime.airingStart === "null") "Unknown" else upcomingAnime.airingStart
            title.text = upcomingAnime.title
            year.text = airingStart;

            Picasso.get()
                    .load(upcomingAnime.imageUrl)
                    .fit()
                    .into(itemView.upcoming_anime_item_poster)
        }

    }
}
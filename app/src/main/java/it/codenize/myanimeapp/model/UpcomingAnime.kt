package it.codenize.myanimeapp.model
import com.google.gson.annotations.SerializedName

class UpcomingAnime(@SerializedName("mal_id")
                    var malId: Number,
                    var rank: Number,
                    var url: String,
                    @SerializedName("image_url")
                    var imageUrl: String,
                    var title: String,
                    var type: String,
                    var score: Number,
                    var members: Number,
                    @SerializedName("airing_start")
                    var airingStart: String,
                    @SerializedName("airing_end")
                    var airingEnd: String,
                    var episodes: Number) {

    override fun toString(): String {
        return "Anime(malId=$malId, " +
                "rank=$rank, " +
                "url='$url', " +
                "imageUrl='$imageUrl', " +
                "title='$title', " +
                "type='$type', " +
                "score=$score, " +
                "members=$members, " +
                "airingStart='$airingStart', " +
                "airingEnd='$airingEnd', " +
                "episodes=$episodes, "
    }
}
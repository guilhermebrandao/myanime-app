package it.codenize.myanimeapp.model

import com.google.gson.annotations.SerializedName

class UpcomingAnimeSearchResult(@SerializedName("top") var upcomingAnimes: List<UpcomingAnime>) {

    override fun toString(): String {
        return upcomingAnimes.toString();
    }

}
package it.codenize.myanimeapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import it.codenize.myanimeapp.adapter.UpcomingAnimesAdapter
import it.codenize.myanimeapp.http.UpcomingAnimeService
import it.codenize.myanimeapp.model.UpcomingAnime
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var disposable: Disposable? = null


    private val upcomingAnimeService by lazy {
        UpcomingAnimeService.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fetchUpcomingAnimes()
    }

    private fun setManager(upcomingAnimes: List<UpcomingAnime>) {
        val layoutManager = GridLayoutManager(this, 2)
        val recyclerView = anime_list_recyclerview
        recyclerView.adapter = UpcomingAnimesAdapter(upcomingAnimes, this)
        recyclerView.layoutManager = layoutManager
    }


    private fun fetchUpcomingAnimes() {
        disposable = upcomingAnimeService.getUpcomingAnimes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    setManager(result.upcomingAnimes)
                }, { error ->
                    error.printStackTrace()
                })
    }


    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}

package it.codenize.myanimeapp.http

import io.reactivex.Observable
import it.codenize.myanimeapp.model.UpcomingAnimeSearchResult
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface UpcomingAnimeService {

    @GET("top/anime/1/upcoming")
    fun getUpcomingAnimes(): Observable<UpcomingAnimeSearchResult>

    companion object {
        fun create(): UpcomingAnimeService {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(
                            GsonConverterFactory.create())
                    .baseUrl("https://api.jikan.moe/")
                    .build()

            return retrofit.create(UpcomingAnimeService::class.java)
        }
    }
}